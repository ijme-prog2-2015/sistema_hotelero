
from PyQt4.QtGui import QApplication
from PyQt4.uic import loadUi
from about.about import AcercaDe
from reservas.nueva_reserva import DlgNuevaReserva
from clientes.nuevo_cliente import DlgNuevoCliente
from habitaciones.nueva_habitacion import D1gNuevaHabitacion
from tipohabitacion.tipo_habitacion import DlgTipoHabitacion
from modelo.hotel import Hotel

class Sistema(QApplication):

    def __init__(self):
        super().__init__([])
        self.ui = loadUi('main.ui')
        self.hotel = Hotel()
        self.ui.actionAcerca_de.triggered.connect(self.onAcercaDe)
        self.ui.btnNuevaReserva.clicked.connect(self.onNuevaReserva)
        self.ui.btnNuevoCliente.clicked.connect(self.onNuevoCliente)
        self.ui.btnNuevaHabitacion.clicked.connect(self.onNuevaHabitacion)
        self.ui.btnTipoHabitacion.clicked.connect(self.onTipoHabitacion)
        self.ui.show()

    def onAcercaDe(self):
        self.about = AcercaDe()

    def onNuevoCliente(self):
        self.nuevo_cliente = DlgNuevoCliente(self.hotel)

    def onNuevaReserva(self):
        self.nueva_reserva = DlgNuevaReserva(self.hotel)

    def onNuevaHabitacion(self):
        self.nueva_habitacion = D1gNuevaHabitacion(self.hotel)

    def onTipoHabitacion(self):
        self.tipo_habitacion = DlgTipoHabitacion()

sis = Sistema()
sis.exec()
