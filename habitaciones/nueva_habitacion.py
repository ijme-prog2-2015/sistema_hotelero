from PyQt4.uic import loadUi

class D1gNuevaHabitacion:

    def __init__(self,unHotel):
        self.hotel = unHotel
        self.ui = loadUi('habitaciones/nueva_habitacion.ui')
        self.ui.buttonBox.accepted.connect(self.guardar)
        self.ui.show()

    def guardar(self):
        nroHabitacion = self.ui.txtNrohabitacion.text()
        tipoHabitacion = self.ui.comboBox.currentText()
        print("Guardando", nroHabitacion, tipoHabitacion)
        self.hotel.guardarHabitacion(nroHabitacion, tipoHabitacion)
