from PyQt4.uic import loadUi

class DlgTipoHabitacion:

    def __init__(self,unHotel):
        self.unHotel = unHotel
        self.ui = loadUi ('tipohabitacion/tipo_habitacion.ui')
        self.ui.buttonBox.accepted.connect(self.guardar)
        self.ui.show()

    def guardar(self):
        codigoHabitacion = self.ui.txtCodigoHabitacion.text()
        descripcion = self.ui.txtDescripcion.text()
        self.hotel.guardarTipoHabitacion(codigoHabitacion, descripcion)
