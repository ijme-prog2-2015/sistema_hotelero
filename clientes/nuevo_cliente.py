from PyQt4.uic import loadUi

class DlgNuevoCliente:

    def __init__(self, unHotel):
        self.hotel = unHotel
        self.ui = loadUi('clientes/nuevo_cliente.ui')
        self.ui.buttonBox.accepted.connect(self.guardar)
        self.ui.show()
        
    def guardar(self):
        dni = self.ui.txtDni.text()
        nombre = self.ui.txtNombre.text()
        apellido = self.ui.txtApellido.text()
        print("Guardando", dni, nombre, apellido)
        self.hotel.guardarCliente(dni, nombre, apellido)

