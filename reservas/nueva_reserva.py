from PyQt4.uic import loadUi

class DlgNuevaReserva:

    def __init__(self,unHotel):
        self.hotel = unHotel
        self.ui = loadUi('reservas/nueva_reserva.ui')
        self.ui.buttonBox.accepted.connect(self.guardarReserva)
        self.ui.show()



    def guardarReserva(self):
        dni_cliente = self.ui.txtDniCliente.text()
        nro_habitacion = self.ui.txtHabitacion.text()
        fecha_desde = self.ui.dateDesde.date().toString()
        fecha_hasta = self.ui.dateHasta.date().toString()
        self.hotel.guardarReserva(dni_cliente, fecha_desde, fecha_hasta, nro_habitacion)
        print ('Reserva guardada.')
